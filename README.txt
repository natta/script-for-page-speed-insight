A PHP script to look up and parse Google Pagespeed results
Key variables:
1. $urls = array('https://www.domain1.com', 'https://www.domain2.com');
    URLs you want to track (could be multiple sites or multiple pages on a single site)
    If you add too many pages the script may need more time for execution

2. $totest = 1;    - Set to 1 for desktop, 2 for both desktop and mobile 

3. for ($i = 0; $i < 3; $i++) { //  - how many requests you want to perform for every strategy of every URL, e.g 3 times.

4. $download_dir = 'pagespeed-results.csv';   
       - path to the file for loading the results. It can be a CSV or text file and it will collect all the data. 
         You can delete this file or delete its contents if you no longer need the information.

