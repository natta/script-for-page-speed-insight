<?php
// --- URLs you want to track (could be multiple sites or multiple pages on a single site)

//$urls = array('https://www.domain1.com', 'https://www.domain2.com');
$urls = array('https://ezd8-stage.skyy.io/');
$key = 'AIzaSyAIhJnGQvWtqSWnatoH0PWLW7FgalAVBZM';
$download_dir = 'pagespeed-results.csv'; //path to the file for loading the results
$file = fopen($download_dir, 'a');

foreach ($urls as $url) {
    $score = array();
    $strategy = "";
    $totest = 2;    // Set to 1 for desktop, 2 for both desktop and mobile
    for ($ii = 0; $ii < $totest; $ii++) { 
        $strategy = ($ii == 0) ? "desktop" : "mobile";
        $columnTitleRow = "Time, URL, Strategy, First Contentful Paint, Speed Index, Largest Contentful Paint, Time to Interactive, Total Blocking Time, Cumulative Layout Shift, Calculated Score\n";
        fwrite($file, $columnTitleRow);
        for ($i = 0; $i < 3; $i++) { // how many requests you want to perform for every strategy of every URL
            $output = "";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' . $url . '&key='.$key.'&strategy=' . $strategy);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
            $result = curl_exec($ch);
            $data = json_decode($result, true);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            } else {
                $date = $data['analysisUTCTimestamp'];
                $output .= $date . ', ' . $url . ', ' . $strategy . ',';
                $fcp = $data['lighthouseResult']['audits']['first-contentful-paint']['score'] * 100;
                $fcp_value = $data['lighthouseResult']['audits']['first-contentful-paint']['displayValue'];
                $output .= $fcp_value . ',';
                $si = $data['lighthouseResult']['audits']['speed-index']['score'] * 100;
                $si_value = $data['lighthouseResult']['audits']['speed-index']['displayValue'];
                $output .= $si_value . ',';
                $lcp = $data['lighthouseResult']['audits']['largest-contentful-paint']['score'] * 100;
                $lcp_value = $data['lighthouseResult']['audits']['largest-contentful-paint']['displayValue'];
                $output .= $lcp_value . ',';
                $tti = $data['lighthouseResult']['audits']['interactive']['score'] * 100;
                $tti_value = $data['lighthouseResult']['audits']['interactive']['displayValue'];
                $output .= $tti_value . ',';
                $tbt = $data['lighthouseResult']['audits']['total-blocking-time']['score'] * 100;
                $tbt_value = $data['lighthouseResult']['audits']['total-blocking-time']['displayValue'];
                $output .= $tbt_value . ',';
                $cls = $data['lighthouseResult']['audits']['cumulative-layout-shift']['score'] * 100;
                $cls_value = $data['lighthouseResult']['audits']['cumulative-layout-shift']['displayValue'];
                $output .= $cls_value . ',';
                $calc = $fcp * 15 / 100 + $si * 15 / 100 + $lcp * 25 / 100 + $tti * 15 / 100 + $tbt * 25 / 100 + $cls * 5 / 100;
                $output .= $calc . "\n";
                fwrite($file, $output);
                $score[$strategy][$i] = $calc;
                @curl_close($ch);
            }
        }
        $aver = round(array_sum($score[$strategy]) / count($score[$strategy]));
        fwrite($file, 'average score = ' . $aver . "\n");
    }
}
?>
